window.onload = function(){
    const token = sessionStorage.getItem("token");
    if(!token){
        window.location.href = "./index.html";
    }
}
document.getElementById("review-form").addEventListener("submit", createReview);

function createReview(event){
    event.preventDefault();

    const name = document.getElementById("inputProductName").value;
    const brand = document.getElementById("inputProductBrand").value;
    const type = document.getElementById("inputDrinkType").value;

    
    const token = sessionStorage.getItem("token");

    const productObj = {"name": name, "type": type, "brand": brand};

    
    const productJson = JSON.stringify(productObj);
    ajaxCreateProduct(productJson, token, getProductId, getProductFail);

    // console.log(reviewJson);
    // console.log(productJson);

    function getProductId(xhr){
        const product = xhr.responseText;
        const productObj = JSON.parse(product);
        const rating = document.getElementById("inputRating").value;
        const review = document.getElementById("inputReviewText").value;
        const userId = sessionStorage.getItem("userId");

        const reviewObj = {"score": rating, "reviewText": review, "accountId": userId, "productId": productObj.productId};
        const reviewJson = JSON.stringify(reviewObj);

        ajaxCreateReview(reviewJson, token, reviewCreated, getProductFail);
    }

    function reviewCreated(xhr){
        window.location.href= "./reviews.html";
    }

    function getProductFail(){
        console.log("fail");
    }
}