document.getElementById("searchBar").addEventListener("submit", attemptSearch);

function attemptSearch(event){
    event.preventDefault();

    const item = document.getElementById("searchInput").value;
    sessionStorage.setItem("item",item);
    window.location.href="searchedreview.html";
}