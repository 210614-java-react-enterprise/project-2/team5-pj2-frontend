document.getElementById("signup-form").addEventListener("submit", attemptSignUp);

function attemptSignUp(event){
    event.preventDefault();

    const username = document.getElementById("inputSignUpUsername").value;
    const password = document.getElementById("inputSignUpPassword").value;

    console.log(username);
    console.log(password);

    const user = {"username": username, "userPass": password};
    const userJson = JSON.stringify(user);

    ajaxSignUp(userJson, signUpSuccess, loginFail);
}

function signUpSuccess(xhr){
    const userObj = xhr.responseText;
    const user = JSON.parse(userObj);
    console.log("signed up")
    const password = document.getElementById("inputSignUpPassword").value;
    ajaxLogin(user.username, password, loginSuccess, loginFail);
}

function loginSuccess(xhr){
    console.log("Success");
    let token = xhr.getResponseHeader("Authorization");
    console.log(token);
    sessionStorage.setItem("token", token);
    console.log(sessionStorage.getItem("token"));
    ajaxGetUserByUsername(token, document.getElementById("inputUsername").value, setId, loginFail);

}

function setId(xhr){
    const user = xhr.responseText;
    const userJson = JSON.parse(user);
    sessionStorage.setItem("userId",userJson.accountId);
    sessionStorage.getItem("userId");
    window.location.href = "./index.html";

}

function loginFail(){
    console.log("Failed");
}