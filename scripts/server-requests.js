function sendAjaxRequest(method, url, body, requestHeaders, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    for(let requestHeaderKey in requestHeaders){
        // {"Authorization" : "auth-token", "Content-Type": "application/json"}
        xhr.setRequestHeader(requestHeaderKey, requestHeaders[requestHeaderKey]);
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
          console.log(xhr);
        if (xhr.status>199 && xhr.status<300) {
          successCallback(xhr);
        } else {
          failureCallback(xhr);
        }
      }
    };
    if(body){
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxGet(url, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("GET", url, null, requestHeaders, successCallback, failureCallback);
}

function sendAjaxDelete(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("DELETE",url, body, requestHeaders, successCallback, failureCallback);
}

function ajaxGetUsers(authToken, id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/id/${id}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetReviews(authToken, successCallback, failureCallback){
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/reviews";
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetReviewById(authToken,id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/reviews/${id}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetReviewsByProductId(authToken, id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/reviews/product/${id}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetProduct(authToken,name, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/products/name/${name}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function sendAjaxPost(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("POST",url, body, requestHeaders, successCallback, failureCallback);
}

function ajaxCreateReview(body, authToken, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/reviews";
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded"};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/login";
    let requestBody = `username=${username}&password=${password}`;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}

function ajaxSignUp(body, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json"};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/signup";
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxGetCommentsByReviewId(authToken, id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/comments/review/${id}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetUserByUsername(authToken, string, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/${string}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxCreateComment(body, authToken, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/comments";
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxCreateProduct(body, authToken, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/products";
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxCreateFavorite(body, id, authToken, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/id/${id}/favorites`;
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxGetProductById(authToken, id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/products/${id}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetFavoriteById(authToken, id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/id/${id}/favorites`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxDeleteComment(authToken, body, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/comments";
    sendAjaxDelete(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxDeleteReview(authToken, body, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/reviews";
    sendAjaxDelete(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxDeleteFavorite(authToken, body, id, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/id/${id}/favorites`;
    sendAjaxDelete(requestUrl, body, requestHeaders, successCallback, failureCallback);
}
function ajaxGetUserById(authToken, id, successCallback, failureCallback){
    let requestUrl = `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/id/${id}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}