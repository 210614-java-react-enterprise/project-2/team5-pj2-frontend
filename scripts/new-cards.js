var accountIds = [];
var productIds = [];
window.onload = function () {
    const token = sessionStorage.getItem("token");
    if(token){
        ajaxGetReviews(token, renderReviews, logError);
    } else {
        window.location.href = "./index.html";
    }
  };
  
  function renderReviews(xhr) {
    const reviewJson = xhr.responseText;
    const reviews = JSON.parse(reviewJson);
    reviews.forEach(review => {
        accountIds.push(review.accountId);
        productIds.push(review.productId);
    });
    const reviewHtml = reviews
      .map(
        (review) =>
          `<div class="container my-3"><div class="card">
            <div class="card-header py-3" style="background-color: #8b0000;"></div>
            <div class="card-body">
                <div class="col-2">
                    <a href="review.html?id=${review.reviewId}" id="cardLink"><h5 class="card-title card-header-${review.productId}">Review Details</h5></a>
                </div>
                <div class="container">
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Et magnis dis parturient montes nascetur.
                        Etiam erat velit scelerisque in dictum non consectetur a erat.</p>
                        <p>Rating: ${stars(review.score)}</p>
                    <blockquote class="blockquote mb-0">
                        <div class="blockquote-footer card-title-${review.accountId}">Someone famous in</div>
                    </blockquote>
                </div>
            </div>
            <div class="card-footer py-3" style="background-color: #8b0000;"></div>
        </div></div>`
      )
      .join("");
    document.getElementById("cardItems").innerHTML = reviewHtml;
    userIds(accountIds);
    addProductIds(productIds);
  }

  function logError(){
    console.log("there was an issue with the request");
  }

  function userIds(ids){
    const token = sessionStorage.getItem("token");
    ids.forEach(id => {
        ajaxGetUsers(token, id, addUser, logError);
    })
  }

  function addUser(xhr){
      const userJson = xhr.responseText;
      const user = JSON.parse(userJson);
      const div = document.getElementsByClassName(`card-title-${user.accountId}`);
      for (item of div){
        item.innerText = user.username;
    }
  }

  function addProductIds(ids){
    const token = sessionStorage.getItem("token");
    ids.forEach(id => {
        ajaxGetProductById(token, id, addProduct, logError);
    })
}

function addProduct(xhr){
    const productJson = xhr.responseText;
    const product = JSON.parse(productJson);
    const div = document.getElementsByClassName(`card-header-${product.productId}`);
    // .innerText = product.brand+" "+product.name;
    for (item of div){
        item.innerText = product.brand+": "+product.name; 
    }
}

function stars(num){
    switch(num){
        case 1:
            return(`<i class="fas fa-beer"></i>`);
        case 2:
            return(`<i class="fas fa-beer"></i> <i class="fas fa-beer"></i>`);
        case 3:
            return(`<i class="fas fa-beer"></i> <i class="fas fa-beer"></i> <i class="fas fa-beer"></i>`);
        case 4:
            return(`<i class="fas fa-beer"></i> <i class="fas fa-beer"></i> <i class="fas fa-beer"></i> <i class="fas fa-beer"></i>`);
        case 5:
            return(`<i class="fas fa-beer"></i> <i class="fas fa-beer"></i> <i class="fas fa-beer"></i> <i class="fas fa-beer"></i> <i class="fas fa-beer"></i>`);
    }
}
