window.onload = function () {
    const url = window.location.search;
    const urlParams = new URLSearchParams(url);
    const id = urlParams.get('id');
    const token = sessionStorage.getItem("token");
    if(token){
        ajaxGetReviewById(token, id, renderReviews, logError);
    } else {
        window.location.href = "./home.html";
    }
  };
  
  function renderReviews(xhr) {
    const reviewJson = xhr.responseText;
    const review = JSON.parse(reviewJson);
    const accountIds = review.accountId;
    const productIds = review.productId;
    const reviewId = review.reviewId;
    const reviewHtml = `<div class="container my-2" style="border-style: solid; background-color: #8b0000;">
        <div class="row">
            <h1 class="py-2" style="text-align: center; text-decoration: underline;">Review Details</h1>
        </div>
        <div class="row">
            <h5 id="username-header-${review.accountId}"style="text-align: center;">username</h5>
        </div>
        <div class="row">
            <h6 id="product-header-${review.productId}"style="text-align: center;">username</h6>
        </div>
        <div class="container-fluid mb-2" id="reviewContainer">
            <div class="row justify-content-center m-0" id="reviewText">
                <div class="col-10">
                <p>${review.reviewText}</p>
                </div>
            </div>
        </div>

        <div class="container" id="reviewContainer">
            <div class="row justify-content-center py-3" id="productLinks">
                
            </div>
            <div class="row pb-2" style="width: 20%; align-self: center;" id="deleteButton">

            </div>
        </div>
    </div>`
    document.getElementById("reviewTitle").innerHTML = reviewHtml;
    userIds(accountIds);
    addProductIds(productIds);
    setFavButton();
    // getLinks("q=captain+morgan&num=3");
    getComments(reviewId);
    createDeleteButton(review);
  }

  function logError(){
    console.log("there was an issue with the request");
  }

  function setFavButton(){
      const accountId = sessionStorage.getItem('userId');
      const token = sessionStorage.getItem('token');
      ajaxGetFavoriteById(token, Number(accountId), getAccounts, logError);
  }

  function getAccounts(xhr){
      const token = sessionStorage.getItem('token');
      const url = window.location.search;
      const urlParams = new URLSearchParams(url);
      const id = urlParams.get('id');
      const favs = xhr.responseText;
      const favObj = JSON.parse(favs);
      ajaxGetReviewById(token, id, setColor, logError);
      function setColor(xhr){
          const review = xhr.responseText;
          const reviewObj = JSON.parse(review);
          for(fav of favObj){
              if(fav.productId == reviewObj.productId){
                //   console.log(fav.accountId + " " + reviewObj.accountId);
                //   console.log(fav.productId + " " + reviewObj.productId);
                  document.getElementById("heart").style.color = "red";
                  break;
              }else{
                //   alert(fav.productId + " " + reviewObj.productId);
              }
          }
      }
  }

  function userIds(id){
      const token = sessionStorage.getItem("token");
      ajaxGetUsers(token, id, addUser, logError);
    // const xhr = new XMLHttpRequest();
    // xhr.open("GET", `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/users/id/${id}`);
    // xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    // xhr.setRequestHeader("Authorization", "Greg");
    // xhr.onreadystatechange = function(){
    //     if(xhr.readyState === 4 && xhr.status === 200){
    //         addUser(xhr);
    //     }else if(xhr.readyState === 4){
    //         console.log("there was an issue with the request");
    //     }
    // }
    // xhr.send();
  }

  function addUser(xhr){
      const userJson = xhr.responseText;
      const user = JSON.parse(userJson);
      document.getElementById(`username-header-${user.accountId}`).innerText = user.username;
  }

  function addProductIds(id){
    const token = sessionStorage.getItem("token");
    ajaxGetProductById(token, id, addProduct, logError);
    // const xhr = new XMLHttpRequest();
    // xhr.open("GET", `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/products/${id}`);
    // xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    // xhr.setRequestHeader("Authorization", "Greg");
    // xhr.onreadystatechange = function(){
    //     if(xhr.readyState === 4 && xhr.status === 200){
    //         addProduct(xhr);
    //     }else if(xhr.readyState === 4){
    //         console.log("there was an issue with the request");
    //     }
    // }
    // xhr.send();
}

function addProduct(xhr){
    const productJson = xhr.responseText;
    const product = JSON.parse(productJson);
    const string = product.brand+": "+product.name;
    const words = product.name.split(' ');
    const query = words.join("+");
    const words2 = product.brand.split(' ');
    const query2 = words2.join("+");
    const wholeQuery = "q=" + query + "+" + query2 + "&num=3";
    getLinks(wholeQuery);
    console.log(document.getElementById(`product-header-${product.productId}`).innerText = string);
}

function getLinks(string){
    const xhr = new XMLHttpRequest();
    xhr.open("GET", `http://ec2-13-59-54-215.us-east-2.compute.amazonaws.com:8081/api/${string}`);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.setRequestHeader("Authorization", "Greg");
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4 && xhr.status === 200){
            addLinks(xhr);
        }else if(xhr.readyState === 4){
            console.log("there was an issue with the request");
        }
    }
    xhr.send();
    
}

function addLinks(xhr){
    const links = xhr.responseText;
    const listOfLinks = JSON.parse(links);
    const linksHtml = listOfLinks.map((link) =>
        `<div class="col-3">
            <div class="card" >
                <img class="card-img-top" src="..." alt="Card image cap" width="40" height="auto">
                <div class="card-body" >
                    <h5 id="link-title">${link.title}</h5>
                    <p id="link-description">${link.description}</p>
                </div>
                <div class="card-footer cardFooter">
                    <a target="_blank" id="gotolink" href="${link.link}"><p>Purchase from here!</p></a>
                </div>
            </div>
        </div>`
    ).join("");
    document.getElementById("productLinks").innerHTML = linksHtml;
}

function getComments(){
    const url = window.location.search;
    const urlParams = new URLSearchParams(url);
    const id = urlParams.get('id');
    const token = sessionStorage.getItem("token");
    ajaxGetCommentsByReviewId(token, id, addComments, logError);
}

function addComments(xhr){
    const comments = xhr.responseText;
    const listOfComments = JSON.parse(comments);
    const commentsHtml = listOfComments.map((comment) =>
    `
    <div class="card my-2">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <img id="avatar" src="images/avatar.jpg" alt="Avatar.png" width="50" height="50">
                </div>
                <p>${comment.comment}</p>
                <div class="text-end">
                <a href="./delete.html?id=${comment.reviewId}&cId=${comment.commentId}&aId=${comment.accountId}&c=${comment.comment}&type=comment" class="btn btn-danger" type="button">X</button></a>
            </div>
        </div>
    </div>`
    ).join("");
    // let a = document.createElement("a");
    // a.setAttribute("id","commentButton");
    // a.setAttribute("role","button");
    // a.addEventListener("click", displayModal);
    // a.innerHTML = commentsHtml;
    // document.getElementById("comments").appendChild(a);
    document.getElementById("comments").innerHTML = commentsHtml;
}

function createDeleteButton(review){
    if(sessionStorage.getItem('userId') == review.accountId){
        document.getElementById('deleteButton').innerHTML = `<a class="btn btn-danger text-center" role="button" href="./delete.html?id=${review.reviewId}&aId=${review.accountId}&pId=${review.productId}&s=${review.score}&r=${review.reviewText}&type=review">Delete Review</a>`
    }
}


function displayModal(){
    $('#deleteCommentModal').modal('show');
}