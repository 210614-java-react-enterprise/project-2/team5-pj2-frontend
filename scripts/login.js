document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event){
    event.preventDefault();

    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;

    ajaxLogin(username, password, loginSuccess, loginFail);
}

function loginSuccess(xhr){
    console.log("Success");
    let token = xhr.getResponseHeader("Authorization");
    console.log(token);
    sessionStorage.setItem("token", token);
    console.log(sessionStorage.getItem("token"));
    ajaxGetUserByUsername(token, document.getElementById("inputUsername").value, setId, loginFail);

}

function setId(xhr){
    const user = xhr.responseText;
    const userJson = JSON.parse(user);
    sessionStorage.setItem("userId",userJson.accountId);
    sessionStorage.getItem("userId");
    window.location.href = "./index.html";

}

function loginFail(){
    console.log("Failed");
}
