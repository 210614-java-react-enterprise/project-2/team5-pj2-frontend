window.onload = function (){
    const login = document.getElementById("loginElement");
    const logout = document.getElementById("logoutElement");
    const token = sessionStorage.getItem("token");
    console.log(token);
    if(sessionStorage.getItem('token')){
        logout.style.display = "block";
        login.style.display= "none";
    }else{
        login.style.display = "block";
        logout.style.display = "none";
    }
}

$(function(){
    $('#loginButton').click(function(){
        $('#loginModal').modal('show');
    });

    $('#deleteComment').click(function(){
        $('#deleteCommentModal').modal('show');
    });

    $('#logoutButton').click(function(){
        sessionStorage.clear();
        window.location.href = "./index.html";
    })

    $(document).on('click', '#addFavorite', function(){
        if(document.getElementById('heart').style.color == "red"){
            const token = sessionStorage.getItem('token');
            const url = window.location.search;
            const urlParams = new URLSearchParams(url);
            const id = urlParams.get('id');
            // console.log(id);
            // alert(id);
            ajaxGetReviewById(token, id, getProductToDelete, getFailure);
        }else{
            const token = sessionStorage.getItem('token');
            const url = window.location.search;
            const urlParams = new URLSearchParams(url);
            const id = urlParams.get('id');
            // console.log(id);
            // alert(id);
            ajaxGetReviewById(token, id, getProduct, getFailure);
        }
     
    });

    function getProductToDelete(xhr){
        const token = sessionStorage.getItem('token');
        const reviewResp = xhr.responseText;
        const reviewObj = JSON.parse(reviewResp);
        const accountId = sessionStorage.getItem('userId')
        const favoriteId = {"accountId": Number(accountId), "productId": Number(reviewObj.productId)};
        const favoriteIdJson = JSON.stringify(favoriteId);
        ajaxDeleteFavorite(token, favoriteIdJson, Number(accountId), deletedFavorite, getFailure);
    }

    function getProduct(xhr){
        const token = sessionStorage.getItem('token');
        const reviewResp = xhr.responseText;
        const reviewObj = JSON.parse(reviewResp);
        const accountId = sessionStorage.getItem('userId')
        const favoriteId = {"accountId": Number(accountId), "productId": Number(reviewObj.productId)};
        const favoriteIdJson = JSON.stringify(favoriteId);
        ajaxCreateFavorite(favoriteIdJson, Number(accountId), token, createdFavorite, getFailure);
    }

    function createdFavorite(){
        document.getElementById("heart").style.color = "red";
    }

    function deletedFavorite(){
        document.getElementById("heart").style.color = "#0275d8";
    }

    function getFailure(){
        document.getElementById("error").innerText = "There was an issue with the favorited items.";
        $('#myModal').modal('show');
    }

    // $('#addFavorite').on('click', '#addComment', function(){
    //     console.log("Add");
    // })

    // $(document).ready(function(){
    //     $('#addFavorite').click(function(){
    //         console.log("add");
    //     })
    // })

    // $('#deleteCommentButton2').click(function(){
    //     $('#deleteCommentModal').modal('show');
    // });

    $('#addComment').click(function(){
        $('#commentModal').modal('show');
    });

    $('#cancelModal').click(function(){
        $('#commentModal').modal('hide');
        $('#myModal').modal('hide');
    })

    $('#closeModal').click(function(){
        $('#commentModal').modal('hide');
        $('#myModal').modal('hide');
    })

    $('#cancelModal').click(function(){
        $('#loginModal').modal('hide');
    })

    $('#closeModal').click(function(){
        $('#loginModal').modal('hide');
    })

    $('.dropdown-toggle').dropdown();
})