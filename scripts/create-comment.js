document.getElementById("login-form").addEventListener("submit", createComment);

function createComment(event){
    event.preventDefault();

    const token = sessionStorage.getItem('token');
    const comment = document.getElementById("newComment").value;
    const accountId = sessionStorage.getItem("userId");
    const url = window.location.search;
    const urlParams = new URLSearchParams(url);
    const id = urlParams.get('id');

    const commentObj = {"comment": comment, "accountId": Number(accountId), "reviewId": Number(id)};

    const commentJson = JSON.stringify(commentObj);
    console.log(commentJson);
    
    ajaxCreateComment(commentJson, token, createSuccess, createFail);
    // ajaxLogin(username, password, loginSuccess, loginFail);
}

function createSuccess(xhr){
    const url = window.location.search;
    window.location.href = url;
}

function createFail(){
    console.log("fail");
}