window.onload = function () {
    const url = window.location.search;
    const urlParams = new URLSearchParams(url);
    const type = urlParams.get('type');
    if(type == "comment"){
        const id = urlParams.get('id');
        const cId = urlParams.get('cId');
        const aId = urlParams.get('aId');
        const c = urlParams.get('c');
        if(sessionStorage.getItem('userId') == aId){
            const token = sessionStorage.getItem("token");
            const comment = {"comment": c, "commentId": Number(cId), "accountId": Number(aId), "reviewId": Number(id)}
            const commentJson = JSON.stringify(comment);
            ajaxDeleteComment(token, commentJson, redirectUser, deleteFail);
        }else{
            history.back();
        }
        
    }else if(type == "review"){
        const id = urlParams.get('id');
        const pId = urlParams.get('pId');
        const aId = urlParams.get('aId');
        const r = urlParams.get('r');
        const s = urlParams.get('s');
        const token = sessionStorage.getItem("token");
        const review = {"reviewId": id, "score": Number(s), "reviewText": r, "accountId": Number(aId), "productId": Number(pId)}
        const reviewJson = JSON.stringify(review);
        ajaxDeleteReview(token, reviewJson, redirectUser, deleteFail);

    }

    // if(token){
    //     ajaxGetReviewById(token, id, renderReviews, logError);
    // } else {
    //     window.location.href = "./login.html";
    // }
  };


  function redirectUser(){
      window.location.href="./reviews.html";
  }

  function deleteFail() {
      window.location.href="./reviews.html";
  }