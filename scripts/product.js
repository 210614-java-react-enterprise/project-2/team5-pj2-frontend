window.onload = function(){
    const token = sessionStorage.getItem("token");
    if(token){
        const id = sessionStorage.getItem('userId');
        const token = sessionStorage.getItem('token');
        ajaxGetFavoriteById(token, id, getProductIds, logFailure);
        ajaxGetUserById(token, id, addUserTitle, logFailure);
    }else{
        window.location.href="./index.html";
    }

}

function getProductIds(xhr){
    const token = sessionStorage.getItem('token');
    const favs = xhr.responseText;
    const favObjs = JSON.parse(favs);
    for(fav of favObjs){
        ajaxGetProductById(token, fav.productId, getProductInfo, logFailure);
    }
}

let productList = [];
let item;
function getProductInfo(xhr){
    const product = xhr.responseText;
    const productObj = JSON.parse(product);
    const tr = document.createElement("tr");
    tr.innerHTML = `
    <td>${productObj.name}</td>
    <td>${productObj.brand}</td>
    <td>${productObj.type}</td>`
    // const div = document.createElement("div");
    // div.innerHTML = `
    // <div class="col-4">
    //     <div class="card" style="width: 18rem;">
    //         <div class="card-body">
    //         <h5 class="card-title">${productObj.brand}: ${productObj.name}</h5>
    //         <h6 class="card-subtitle mb-2 text-muted">${productObj.type}</h6>
    //         <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    //         </div>
    //     </div>
    // </div>`
    document.getElementById("tableElements").appendChild(tr);
}

function addUserTitle(xhr){
    const user = xhr.responseText;
    const userObj = JSON.parse(user);
    document.getElementById('userTitle').innerText = userObj.username;
}


function logFailure(){
    console.log("Fail");
}